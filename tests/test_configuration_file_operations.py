#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2018 Bitergia
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Authors:
#     Luis Cañas-Díaz <lcanas@bitergia.com>
#

import configparser
import os
import sys
import unittest
import unittest.mock as mock


from indexwarrior.main import IndexWarrior
from indexwarrior.errors import InvalidConfigurationFileError, NotFoundConfigurationFileError, \
    MissingShortcutConfigurationFileError

if '..' not in sys.path:
    sys.path.insert(0, '..')


def read_file(filename, mode='r'):
    """Adhoc function to make reading files easier."""
    with open(os.path.join(os.path.dirname(os.path.abspath(__file__)), filename), mode) as fdesc:
        content = fdesc.read()
        return content


class TestConfigurationFileOperations(unittest.TestCase):
    """Test operations related to the configuration file."""

    @unittest.mock.patch.object(IndexWarrior, 'load_configuration_file')
    def test_get_real_host_valid_file(self, mock_read_config_file):
        """Test with a valid configuration file."""
        content_cfg_valid_format = read_file('data/cfg_file_valid_format')
        config = configparser.ConfigParser()
        config.read_string(content_cfg_valid_format)
        mock_read_config_file.return_value = config

        iw = IndexWarrior('mozilla', 30)
        self.assertEqual(iw.get_real_host('mozilla'), 'https://mafesan:bizcochaco@dulce.biterg.io/data')

    @unittest.mock.patch.object(IndexWarrior, 'load_configuration_file')
    def test_get_real_host_valid_file_missing_shortcut(self, mock_read_config_file):
        """Test with a valid configuration file."""
        content_cfg_valid_format = read_file('data/cfg_file_valid_format')
        config = configparser.ConfigParser()
        config.read_string(content_cfg_valid_format)
        mock_read_config_file.return_value = config

        iw = IndexWarrior('mozilla', 30)
        self.assertRaises(MissingShortcutConfigurationFileError, lambda: iw.get_real_host('doesnotexist'))

    @unittest.mock.patch.object(IndexWarrior, 'load_configuration_file')
    def test_get_real_host_missing_section(self, mock_read_config_file):
        """Test with a configuration file without the shorcuts section."""
        content_cfg_invalid_format = read_file('data/cfg_file_valid_format_missing_section')
        config = configparser.ConfigParser()
        config.read_string(content_cfg_invalid_format)
        mock_read_config_file.return_value = config

        iw = IndexWarrior('mozilla', 30)
        self.assertRaises(InvalidConfigurationFileError, lambda: iw.get_real_host('mozilla'))

    @unittest.mock.patch.object(IndexWarrior, 'load_configuration_file')
    def test_get_real_host_missing_file(self, mock_read_config_file):
        """Test if the file does not exist."""
        mock_read_config_file.side_effect = NotFoundConfigurationFileError
        iw = IndexWarrior('mozilla', 30)
        self.assertRaises(NotFoundConfigurationFileError, lambda: iw.get_real_host('mozilla'))

    # @httpretty.activate
    # def test_elasticsearch_connection(self):
    #     """ """
    #     # wrong credentials
    #     # wrong certificate
    #     # wrong URL
    #     pass
