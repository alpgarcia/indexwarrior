certifi==2016.9.26
colorama==0.3.9
elasticsearch==5.1.0
elasticsearch-dsl==5.1.0
prettytable==0.7.2
httpretty==0.8.14
